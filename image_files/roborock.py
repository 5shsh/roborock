#!/usr/bin/env python
import os
mqttServer=os.environ['MQTT_HOST']

import time
from subprocess import check_output
import paho.mqtt.client as mqtt

import json
import re

def format_status(s):
    status_dic = {
		"Charging" : 1
	}

    lines = s.splitlines()
    status = lines[0].decode('UTF-8').split(' ')[1] 
    battery = lines[1].decode('UTF-8').split(' ')[1] 
    fanspeed = lines[2].decode('UTF-8').split(' ')[1] 
    cleaning_since = lines[3].decode('UTF-8').split(' ')[2] 
    cleaned_area = lines[4].decode('UTF-8').split(' ')[2]

    status_val = status_dic.get(status, -1)
    clock = cleaning_since.split(':')

    cleaning_since_secs = (int(clock[0])*3600) + (int(clock[1])*60) + (int(clock[2]))

    payload = {
        "status": {
            "val": status_val,
            "text": status
        },
        "battery": float(battery),
        "fanspeed": float(fanspeed),
        "cleaning_since": {
            "val": cleaning_since_secs,
            "text": cleaning_since
        },
        "cleaned_area": float(cleaned_area)
    }
    return payload

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))
    client.subscribe("/roborock/get/#")
    client.subscribe("/roborock/set/#")

def on_message(client, userdata, msg):
    print(msg.topic + " " + str(msg.payload))
    if (msg.topic == "/roborock/set/start"):
        print("start")
        out = check_output(['mirobo', 'start'])
    elif (msg.topic == "/roborock/set/stop"):
        print("stop")
        out = check_output(['mirobo', 'stop'])
    elif (msg.topic == "/roborock/set/gohome"):
        print("gohome")
        out = check_output(['mirobo', 'home'])
    elif (msg.topic == "/roborock/get/status"):
        print("status")
        out = check_output(['mirobo', 'status'])
        payload = format_status(out)
        client.publish("/roborock/status", json.dumps(payload))

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect(mqttServer, 1883, 60)
client.loop_forever()