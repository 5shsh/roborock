#-------------------------------------------------------------------------------
# Base Image Spec
#-------------------------------------------------------------------------------
ARG BASE_IMAGE_NAMESPACE=library
ARG BASE_IMAGE_NAME=python
ARG BASE_IMAGE_VERSION=3.6-alpine3.9

FROM ${BASE_IMAGE_NAMESPACE}/${BASE_IMAGE_NAME}:${BASE_IMAGE_VERSION} as builder

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------
#
RUN apk add --no-cache \
    python3-dev \
    g++ \
    libffi-dev \
    musl-dev \
    openssl-dev \
    linux-headers

COPY requirements.txt /
RUN pip3 install --user --no-warn-script-location -r requirements.txt

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-end
RUN [ "cross-build-end" ]

#
# -------
#
FROM ${BASE_IMAGE_NAMESPACE}/${BASE_IMAGE_NAME}:${BASE_IMAGE_VERSION}

#-------------------------------------------------------------------------------
# Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-start
RUN [ "cross-build-start" ]

#-------------------------------------------------------------------------------
# Custom Setup
#-------------------------------------------------------------------------------

COPY --from=builder /root/.local /root/.local

COPY image_files/ .

ENV PATH=/root/.local/bin:$PATH
ENTRYPOINT "/entrypoint.sh"

#-------------------------------------------------------------------------------
# Post Build Environment
#-------------------------------------------------------------------------------
COPY .bin/amd64/nothing.sh /bin/cross-build-end
RUN [ "cross-build-end" ]

#--------------------------------------------------------------------------------
# Labelling
#--------------------------------------------------------------------------------

ARG BUILD_DATE
ARG VCS_REF
ARG VCS_URL
ARG VERSION

LABEL   org.label-schema.name="homesmarthome/roborock" \
        org.label-schema.description="Accepts Roborock commands via MQTT and publish appropriate results back" \
        org.label-schema.url="homesmarthome.5square.de" \
        org.label-schema.vcs-ref="$VCS_REF" \
        org.label-schema.vcs-url="$VCS_URL" \
        org.label-schema.vendor="fvsqr" \
        org.label-schema.version=$VERSION \
        org.label-schema.build-date=$BUILD_DATE \
        org.label-schema.schema-version="1.0" \
        org.label-schema.docker.cmd="TBD"
