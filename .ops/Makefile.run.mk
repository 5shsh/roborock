docker_run:
	docker run -d --name=mosquitto_test_run -p 1883:1883 homesmarthome/mosquitto:latest
	docker run -d \
	  --name=roborock_test_run \
	  	-v $(PWD)/.env:/env \
		-v $(PWD)/image_files/roborock.py:/roborock.py \
		--link mosquitto_test_run:mosquitto \
	  $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker ps | grep roborock_test_run

docker_stop:
	docker rm -f roborock_test_run 2> /dev/null; true
	docker rm -f mosquitto_test_run 2> /dev/null; true